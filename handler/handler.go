package handler

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"
	"wordbot/ent"
	"wordbot/repository"
	"wordbot/spreadsheet"
	"wordbot/twitter"

	"github.com/gin-gonic/gin"
)

func TweetHandler(c *gin.Context) {
	character := convertCharacterID(c)
	maxRetry := 5
	for i := 0; i < maxRetry; i++ {
		words := spreadsheet.FetchWords(character)
		rand.Seed(time.Now().UnixNano())
		selectedWord := words[rand.Intn(len(words))]
		err := twitter.Tweet(character, selectedWord)
		if err != nil {
			continue
		}
		repository.FlushDB()
		c.JSON(http.StatusOK, gin.H{"word": selectedWord.Word, "source": selectedWord.Source})
		return
	}
	c.JSON(http.StatusInternalServerError, gin.H{"message": "最大再試行回数の5回を超えました。"})
}

func CallbackHandler(c *gin.Context) {
	character := convertCharacterID(c)
	code := c.Query("code")
	state := c.Query("state")
	if state != character.State {
		panic(fmt.Sprintf("Different State! url: %s db: %s\n", state, character.State))
	}
	refreshToken := twitter.UpdateRefreshToken(character, code)
	repository.FlushDB()
	c.JSON(http.StatusOK, gin.H{"message": fmt.Sprintf("RefreshToken Updated! RefreshToken: %s", refreshToken)})
}

func AuthURLGenerateHandler(c *gin.Context) {
	fmt.Println(twitter.GenerateAuthURL(convertCharacterID(c)))
	repository.FlushDB()
}

func convertCharacterID(c *gin.Context) *ent.Character {
	id, _ := strconv.Atoi(c.Param("id"))
	if id == 0 {
		panic("")
	}
	client, _ := ent.Open("sqlite3", "file:db.sqlite3?_fk=1")
	defer client.Close()
	character := client.Character.GetX(context.Background(), id)
	if character == nil {
		panic("")
	}
	return character
}
