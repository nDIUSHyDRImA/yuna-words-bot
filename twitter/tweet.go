package twitter

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"wordbot/ent"
	"wordbot/repository"
	"wordbot/spreadsheet"

	"golang.org/x/oauth2"
)

var config *oauth2.Config

type TweetText struct {
	Text string `json:"text"`
}

func initConfig(character *ent.Character) {
	config = &oauth2.Config{
		ClientID:     os.Getenv("TWITTER_CLIENT_ID_" + strconv.Itoa(character.ID)),
		ClientSecret: os.Getenv("TWITTER_CLIENT_SECRET_" + strconv.Itoa(character.ID)),
		RedirectURL:  os.Getenv("TWITTER_REDIRECT_URL_" + strconv.Itoa(character.ID)),
		Scopes:       []string{"users.read", "tweet.read", "tweet.write", "offline.access"},
		Endpoint: oauth2.Endpoint{
			AuthURL:   "https://twitter.com/i/oauth2/authorize",
			TokenURL:  "https://api.twitter.com/2/oauth2/token",
			AuthStyle: oauth2.AuthStyleInHeader,
		},
	}
}

func Tweet(character *ent.Character, selectedWord spreadsheet.Word) error {
	initConfig(character)
	token := makeToken(character)
	tweetTextBytes, err := json.Marshal(TweetText{Text: fmt.Sprintf("%s\n\n%s", selectedWord.Word, selectedWord.Source)})
	if err != nil {
		panic(err)
	}
	req, _ := http.NewRequest("POST", "https://api.twitter.com/2/tweets", bytes.NewBuffer(tweetTextBytes))
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token.AccessToken))
	req.Header.Add("Content-Type", "application/json")
	tryCount := 0
	for tryCount <= 1 {
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		body, _ := io.ReadAll(resp.Body)
		if resp.StatusCode == 403 {
			return fmt.Errorf("Tweet failed. Retry.\n")
		} else if resp.StatusCode >= 400 {
			if tryCount == 0 {
				tryCount += 1
				continue
			}
			panic(string(body))
		}
		log.Println(string(body))
		return nil
	}
	return nil
}

func makeToken(character *ent.Character) *oauth2.Token {
	refreshToken := character.RefreshToken
	form := url.Values{}
	form.Add("refresh_token", refreshToken)
	form.Add("grant_type", "refresh_token")
	form.Add("client_id", config.ClientID)
	req, _ := http.NewRequest("POST", config.Endpoint.TokenURL, strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth(config.ClientID, config.ClientSecret)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		panic(string(body))
	}
	var token oauth2.Token
	err = json.Unmarshal(body, &token)
	if err != nil {
		panic(err)
	}
	updateRefreshToken(character, token.RefreshToken)
	return &token
}

func updateRefreshToken(character *ent.Character, refreshToken string) {
	client, _ := ent.Open("sqlite3", "file:db.sqlite3?_fk=1")
	defer client.Close()
	client.Character.UpdateOne(character).SetRefreshToken(refreshToken).ExecX(context.Background())
	repository.FlushDB()
}
