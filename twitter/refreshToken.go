package twitter

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"math/rand"
	"strings"
	"time"
	"wordbot/ent"

	"github.com/google/uuid"
	"golang.org/x/oauth2"
)

const VERIFIER_LENGTH = 128

func GenerateAuthURL(character *ent.Character) string {
	config := getOAuthConfig(character)
	h := sha256.New()
	codeVerifier := generateCodeVerifier()
	state, _ := uuid.NewRandom()

	client, _ := ent.Open("sqlite3", "file:db.sqlite3?_fk=1")
	defer client.Close()
	client.Character.UpdateOne(character).SetCodeVerifier(codeVerifier).SetState(state.String()).ExecX(context.Background())

	h.Write([]byte(codeVerifier))
	url := config.AuthCodeURL(state.String(), oauth2.AccessTypeOffline, oauth2.SetAuthURLParam("code_challenge", base64.RawURLEncoding.EncodeToString(h.Sum(nil))), oauth2.SetAuthURLParam("code_challenge_method", "S256"))
	url = strings.ReplaceAll(url, "+", "%20")
	return url
}

func generateCodeVerifier() string {
	const charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~"
	verifier := ""
	for i := 0; i < VERIFIER_LENGTH; i++ {
		rand.Seed(time.Now().UnixNano())
		selectedIndex := rand.Intn(len(charset))
		verifier += charset[selectedIndex:selectedIndex + 1]
	}
	return verifier
}

func UpdateRefreshToken(character *ent.Character, code string) string {
	config := getOAuthConfig(character)
	codeVerifier := character.CodeVerifier
	token, err := config.Exchange(context.Background(), code, oauth2.SetAuthURLParam("code_verifier", codeVerifier))
	if err != nil {
		panic(err)
	}
	if token.RefreshToken == "" {
		panic("")
	}
	client, _ := ent.Open("sqlite3", "file:db.sqlite3?_fk=1")
	defer client.Close()
	client.Character.UpdateOne(character).SetRefreshToken(token.RefreshToken).ExecX(context.Background())
	return token.RefreshToken
}
