package twitter

import (
	"os"
	"strconv"
	"wordbot/ent"

	"golang.org/x/oauth2"
)

func getOAuthConfig(character *ent.Character) *oauth2.Config {
	return &oauth2.Config{
		ClientID:     os.Getenv("TWITTER_CLIENT_ID_" + strconv.Itoa(character.ID)),
		ClientSecret: os.Getenv("TWITTER_CLIENT_SECRET_" + strconv.Itoa(character.ID)),
		RedirectURL:  os.Getenv("TWITTER_REDIRECT_URL_" + strconv.Itoa(character.ID)),
		Scopes:       []string{"users.read", "tweet.read", "tweet.write", "offline.access"},
		Endpoint: oauth2.Endpoint{
			AuthURL:   "https://twitter.com/i/oauth2/authorize",
			TokenURL:  "https://api.twitter.com/2/oauth2/token",
			AuthStyle: oauth2.AuthStyleInHeader,
		},
	}
}
