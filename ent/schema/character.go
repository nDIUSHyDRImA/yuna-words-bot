package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// Character holds the schema definition for the Character entity.
type Character struct {
	ent.Schema
}

const (
	YACHIYO = 1 + iota
	YUNA
)

// Fields of the Character.
func (Character) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").NotEmpty().Unique(),
		field.String("refresh_token").Optional(),
		field.String("state"),
		field.String("code_verifier"),
	}
}

// Edges of the Character.
func (Character) Edges() []ent.Edge {
	return nil
}
