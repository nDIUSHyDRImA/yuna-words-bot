package router

import (
	"context"
	"os"
	"wordbot/ent"
	"wordbot/ent/migrate"
	"wordbot/gcp"
	"wordbot/handler"

	"github.com/gin-gonic/gin"
)

func RegisterRoutes() {
	r := gin.Default()
	r.Use(initDB())
	r.POST("/:id/generate-auth-code-url", handler.AuthURLGenerateHandler)
	r.GET("/:id/callback", handler.CallbackHandler)
	r.POST("/:id/tweet", handler.TweetHandler)
	r.Run(":8080")
}

func initDB() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		data := gcp.Download("db.sqlite3")
		os.WriteFile("db.sqlite3", data, 0777)
		client, _ := ent.Open("sqlite3", "file:db.sqlite3?_fk=1")
		defer client.Close()
		if err := client.Schema.Create(context.Background(), migrate.WithDropIndex(true), migrate.WithDropColumn(true)); err != nil {
			panic(err)
		}
		ctx.Next()
	}
}
