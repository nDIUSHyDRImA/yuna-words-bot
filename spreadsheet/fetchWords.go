package spreadsheet

import (
	"context"
	"fmt"
	"log"
	"wordbot/ent"

	"google.golang.org/api/option"

	"google.golang.org/api/sheets/v4"
)

const spreadsheetID = "1n4jQdrBhBFzB4zD9yWgdGkIQPQra9vUEAp5B1qYyo20"

type Word struct {
	Word   string
	Source string
}

func FetchWords(character *ent.Character) []Word {
	credential := option.WithCredentialsFile("credentials/cloudstorage.json")

	srv, err := sheets.NewService(context.TODO(), credential)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := srv.Spreadsheets.Values.Get(spreadsheetID, fmt.Sprintf("%s台詞集!A:B", character.Name)).Do()
	if err != nil {
		log.Fatalln(err)
	}
	if len(resp.Values) == 0 {
		log.Fatalln("data not found")
	}
	words := []Word{}
	for idx, row := range resp.Values {
		if idx == 0 {
			continue
		}
		words = append(words, Word{Word: row[0].(string), Source: row[1].(string)})
	}
	return words
}
