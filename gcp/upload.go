package gcp

import (
	"bytes"
	"context"
	"io"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

func Upload(objectName string, data []byte) {
	buf := bytes.NewBuffer(data)
	client, _ := storage.NewClient(context.Background(), option.WithCredentialsFile("./credentials/cloudstorage.json"))
	defer client.Close()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*50)
	defer cancel()
	wc := client.Bucket("sqlite-9v9kl9noa7").Object(objectName).NewWriter(ctx)
	defer wc.Close()
	wc.ChunkSize = 0
	io.Copy(wc, buf)
}
