package gcp

import (
	"context"
	"io/ioutil"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

func Download(objectName string) []byte {
	client, _ := storage.NewClient(context.Background(), option.WithCredentialsFile("./credentials/cloudstorage.json"))
	defer client.Close()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*50)
	defer cancel()
	rc, _ := client.Bucket("sqlite-9v9kl9noa7").Object(objectName).NewReader(ctx)
	defer rc.Close()
	data, _ := ioutil.ReadAll(rc)
	return data
}
