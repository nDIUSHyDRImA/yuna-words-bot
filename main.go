package main

import (
	"wordbot/router"

	_ "github.com/joho/godotenv/autoload"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	router.RegisterRoutes()
}
