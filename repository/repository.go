package repository

import (
	"os"
	"wordbot/gcp"
)

func FlushDB() {
	data, _ := os.ReadFile("db.sqlite3")
	gcp.Upload("db.sqlite3", data)
}
